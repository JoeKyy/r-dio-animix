<?php
/*
Plugin Name: FK Database Tool
Version: 1.1.0
Plugin URI: http://www.filipekiss.com.br
Description: Plugin usado para salvar dados no banco de dados do Wordpress, em tabelas customizadas.
Author: Filipe Kiss
Author URI: http://www.filipekiss.com.br
*/


Class fkdbt{

	var $table_name;


	/**
	 * Função que cria uma nova tabela no banco de dados do blog atual;
	 *
	 * @param string $table_name (opcional) Nome da Tabela | Caso a tabela já tenha sido passada no construct, ignorar esse campo e passar diretamente o array de campos.
	 * @param array $table_fields Um array contendo todos os campos da estrutura da tabela.
	 * @return void
	 * @author Filipe Kiss
	 */
	function create_table($table_name = '', $table_fields = array()){
		global $wpdb;

		//Checa se uma table foi instanciada no __construct. Se sim, checa se o nome da table é o mesmo ou se foi passado apenas um array (o de campos) e atribui os valores que devem ser atribuidos.
		if( $this->table_name && is_array( $table_name ) ){ //Temos a table e o 'table_name' da função é um array de campos
			$table_fields = $table_name;
			$table_name = $this->table_name;
		}
		else{
			$table_name = $this->_sanitize_table( $table_name );
		}


		$query_str = "CREATE TABLE " . $table_name . " (
			id mediumint(9) NOT NULL AUTO_INCREMENT,
			";
		foreach($table_fields as $column){
			$query_str .= $this->_make_field_query($column).",
			";
		}
		$query_str .= "UNIQUE KEY id (id)
		);";

		// echo $query_str;
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		$this->_debug($query_str);
		dbDelta($query_str);
	}

	/**
	 * Função que sanitiza o nome da tabela (coloca o prefixo caso não exista e remove caracteres inválidos);
	 *
	 * @return string O nome da tabela sanitizado.
	 * @author Filipe Kiss
	 **/
	private function _sanitize_table( $table_name = '' )
	{
	    global $wpdb;

	    $table_name = sanitize_title( $table_name );

	    if( !stristr( $table_name , $wpdb->prefix) ){
			$table_name = $wpdb->prefix.$table_name;
		}

		return $table_name;
	}

	/**
	 * Função que gera a string do SQL baseado num array de detalhes
	 *
	 * @return string Uma string no formato SQL para criação do campo na tabela.
	 * @author Filipe Kiss
	 **/
	private function _make_field_query($field_details = '')
	{
		extract( $field_details );
		if( !$type )
			return false;

		$query_str  = "";

		$query_str .= $name." ".$type;

		if( !isset( $default ) || empty( $default ) || $default == NULL ){
			$query_str .= " ".$this->_get_default( $type );
		}



		$query_str .= (( $null ) ? ' NULL' : ' NOT NULL');

		return $query_str;
	}


	/**
	 * Função utilizada pela classe para gerar alguns defaults padrão.
	 *
	 * @param string $type O tipo do campo;
	 * @return string O Valor default.
	 * @author Filipe Kiss
	 */
	private function _get_default( $type = '' )
	{
		switch( $type ) {
			case 'varchar':
				$response = 'DEFAULT NULL';
			break;
			case 'datetime':
				$response = 'DEFAULT \'CURRENT_TIMESTAMP\'';
			break;
			default:
				$response = false;
			break;
		}

		return $response;
	}

	/**
	 * Insere um grupo de registros numa tabela do Wordpress
	 *
	 * @param array $data Um array formatado com os dados a serem inseridos
	 * @return bool Retorna true caso consiga inserir o registro na tabela; Caso contrário, retorna false.
	 * @author Filipe Kiss
	 */
	function insert($data = ''){
	    global $wpdb;

		if( !$data )
			return false;

		//Let's check if we have a table instanced
		if( $this->table_name ){
			$table_name = $this->table_name;
			$dataset = $data;
		}
		else{ //We don't have, so let's hope they passed it in the array
			$table_name = array_keys( $data );
			$dataset = $data[$table_name[0]];
			$table_name = $this->_sanitize_table( $table_name[0] );
		}

		//It exists. Let's insert the data.

        if( is_array( $dataset ) ){
            return $wpdb->insert( $table_name, $dataset);
        }

		//Oops! No data, let's return false;
        return false;

	}


	function update($data = ''){
	    global $wpdb;

		if( !$data )
			return false;

		//Let's check if we have a table instanced
		if( $this->table_name ){
			$table_name = $this->table_name;
			$dataset = $data;
		}
		else{ //We don't have, so let's hope they passed it in the array
			$table_name = array_keys( $data );
			$dataset = $data[$table_name[0]];
			$table_name = $this->_sanitize_table( $table_name[0] );
		}

		$where = array( 'id' => $dataset['id'] );

		//It exists. Let's update the data.

        if( is_array( $dataset ) ){
            return $wpdb->update( $table_name, $dataset , $where);
        }

		//Oops! No data, let's return false;
        return false;

	}

	function delete( $data = '' ){
			    global $wpdb;

				if( !$data )
					return false;

				//Let's check if we have a table instanced
				if( $this->table_name ){
					$table_name = $this->table_name;
					$dataset = $data;
				}
				else{ //We don't have, so let's hope they passed it in the array
					$table_name = array_keys( $data );
					$dataset = $data[$table_name[0]];
					$table_name = $this->_sanitize_table( $table_name[0] );
				}

				//It exists. Let's update the data.

		        if( is_array( $dataset ) ){
		        	$where = " WHERE ";
		        	foreach( $dataset as $column => $value ){
		        		$where .= $table_name.".".$column." = '" . $value . "' AND ";
		        	}
		        	$where .= "1=1";
		        	$query = $wpdb->prepare( 'DELETE FROM ' . $table_name . $where );
		        	die( $query );
		            return $wpdb->update( $table_name, $dataset , $where);
		        }

				//Oops! No data, let's return false;
		        return false;
	}

	/**
	 * Checa se uma tabela existe no banco de dados do blog atual.
	 *
	 * @param string $table_name O nome da tabela a ser encontrada.
	 * @return bool Retorna true se a tabela existir; Caso contrário, retorna false.
	 * @author Filipe Kiss
	 */
	private function _table_exists($table_name = '')
	{
		global $wpdb;
		return true;
	}

	function __construct( $table_name = '' , $debug = false ) {
		if( $table_name ){
			$this->table_name = $this->_sanitize_table( $table_name );
		}
		else{
			$this->table_name = false;
		}
		$this->debug = $debug;
	}

	/**
	 * Função utilizada para mudar a tabela a ser usada em tempo de execução
	 *
	 * @param string $table_name O Nome da tabela a ser usada. Essa tabela precisa existir no banco.
	 * @return mixed (bool|string) Retorna o nome da tabela caso ela exista no banco ou false
	 * @author Filipe Kiss
	 */
	function use_table( $table_name = '' ){
		return ( $this->table_name = $this->_sanitize_table( $table_name ) );
	}

	/**
	 * Função de ORM simples para retornar registros do database.
	 *
	 * @param string $type O tipo de query a ser efetuada: ALL ou FIRST;
	 * @param array $params Um array de parâmetros SQL para montar a query
	 * @return void
	 * @author Filipe Kiss
	 */
	function find($type = '', $params = array()){

		global $wpdb;

		if( empty($type) )
			return false;

		$t = $this->table_name.".";

		$query_str = "SELECT";

		if($type == 'count')
		{
			$query_str .= " COUNT(".$t."id) ";
		}
		elseif( isset( $params['fields'] ) ){
			$query_str .= " ";
			foreach($params['fields'] as $field => $nickname){
				if( is_string( $field ))
					$fields[] = $t.$field." as ".$nickname;
				else
					$fields[] = $t.$nickname;
			}
			$query_str .= join( ", ", $fields );
		}
		else{
			$query_str .= " ".$t." * ";
		}

		$query_str .= " FROM ".$this->table_name;

		if( isset( $params['conditions'] ) ){

			$query_str .= " WHERE ";
			if( isset( $params['conditions']['OR'] ) ){
				foreach($params['conditions']['OR'] as $field => $condition){
					if( !stristr( $field, " " ) ){
						$condition = ( is_int($condition) ? $condition : "'".$condition."'");
						$where[] = $field." = ".$condition;
					}
					else{
						$condition = ( is_int($condition) ? $condition : "'".$condition."'");
						$where[] = $field." ".$condition;
					}
				}
				$query_str .= join( " OR " , $where );
				unset($params['conditions']['OR']);
			}

			if( isset( $params['conditions']['AND'] ) ){
				foreach($params['conditions']['AND'] as $field => $condition){
					if( !stristr( $field, " " ) ){
						$condition = ( is_int($condition) ? $condition : "'".$condition."'" );
						$where[] = $field." = ".$condition;
					}
					else{
						$condition = ( is_int($condition) ? $condition : "'".$condition."'");
						$where[] = $field." ".$condition;
					}
				}
				$query_str .= join( " AND " , $where );
				unset($params['conditions']['AND']);
			}

			if( !empty( $params['conditions'] ) ){
				foreach($params['conditions'] as $field => $condition){
					if( !stristr( $field, " " ) ){
						$condition = ( is_int($condition) ? $condition : "'".$condition."'");
						$where[] = $field." = ".$condition;
					}
					else{
						$condition = ( is_int($condition) ? $condition : "'".$condition."'");
						$where[] = $field." ".$condition;
					}
				}

				$where = $where[0];
				$query_str .= $where;
			}

		}

		if( isset( $params['order'] ) ){
			$query_str .= " ORDER BY";
			foreach( $params['order'] as $field => $orderBy ){
				if( !is_int($field) ){
					$order[] = $field." ".$orderBy;
				}
				else{
					$order[] = $orderBy." ASC";
				}
			}
			$query_str .= " ".join( ", " , $order);
		}
		if( isset( $params['limit'] ) && $type != 'first' ){
			$current_page = ( $params['page'] ) ? $params['page'] : 1;
			$start = ($current_page - 1) * $params['limit'];
			if( $params['offset'] ){
				$start += ($params['offset'] - 1);
			}
			$query_str .= " LIMIT $start, $params[limit]";
		}
		elseif( $type == "first" ){
			$start = ( isset( $params['offset'] ) ) ? $params['offset'] - 1 : 0;
			$query_str .= " LIMIT $start,1";
		}
		$this->_debug( $query_str );

		if( $type == 'first' ){
			return $wpdb->get_row($query_str, 'OBJECT');
		}
		elseif($type == 'count'){
			return $wpdb->get_var($query_str);
		}
		return $wpdb->get_results($query_str, 'OBJECT');
	}

	function count($params = array()){
		return $this->find('count', $params);
	}

	function model( $table_name = '' ){
		global $wpdb;

		if( $this->table_name && '' == $table_name ){ //Temos a table e o 'table_name' da função é um array de campos
			$table_name = $this->table_name;
		}
		else{
			$table_name = $this->_sanitize_table( $table_name );
		}
		$query = "Select Column_Name From Information_Schema.Columns Where Table_Name = '$table_name' Order By Ordinal_Position";
		$field_names = $wpdb->get_results($query , ARRAY_A );

		$ret_array = array();

		foreach( $field_names as $field){
			$ret_array[$field['Column_Name']] = '';
		}

		return($ret_array);

	}

	function _debug($var = false, $showHtml = false, $showFrom = true) {
			if( !$this->debug ){
				return false;
			}
			if ($showFrom) {
				$calledFrom = debug_backtrace();
				echo '<strong>' . substr(str_replace(ROOT, '', $calledFrom[0]['file']), 1) . '</strong>';
				echo ' (linha <strong>' . $calledFrom[0]['line'] . '</strong>)';
			}
			echo "\n<pre>\n";

			$var = print_r($var, true);
			if ($showHtml) {
				$var = str_replace('<', '&lt;', str_replace('>', '&gt;', $var));
			}
			echo $var . "\n</pre>\n";
	}
}