<?php
/*
Plugin Name: R7 Template Placer
Plugin URI: http://r7.com
Description: Insere um template na sidebar do blog
Version: 1.2
Author: Filipe Kiss
Author URI: http://r7.com
*/



add_action('widgets_init', create_function('', 'return register_widget("r7_tloader_widget");'));

class r7_tloader_Widget extends WP_Widget
{
    protected $_templates = array();

    function r7_tloader_Widget()
    {
		$widget_ops = array('classname' => 'r7-template-placer-widget', 'description' => '');
		$this->WP_Widget('r7-template-placer-widget', 'R7 Template Placer', $widget_ops);
    }

    function widget($args, $instance)
    {
        global $wpdb;
        extract($args);
        $title = apply_filters('widget_title', $instance['title']);
		if(@$instance['templatename']):
			$terms = explode('-',$instance['templatename']);
			echo (@$instance['container']) ? $before_widget : "";
			if($title){
				echo $before_title . $title . $after_title;
			}
			if(file_exists(TEMPLATEPATH."/".$instance['templatename'].".php")):
				?>
					<div class="widget-container">
				<?php
				get_template_part($instance['templatename']);
				$instance['error'] = "";
				?>
					</div>
				<?php
			else:
				$instance['error'] = "O arquivo não existe";
			endif;
			echo (@$instance['container']) ? $after_widget : "";
		endif;
	}

	function form($instance)
    {
		$title = $instance['title'];
		$container = $instance['container'];
		if(file_exists(TEMPLATEPATH."/".$instance['templatename'].".php")):
			$instance['error'] = "";
		else:
			$instance['error'] = "O arquivo não existe";
		endif;
		
		$instance = wp_parse_args((array) $instance, array(
			'title' => '',
			'templatename' => 'slug-name',
			'container' => ''
        ));
    ?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e( 'Title:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>
		
		<p><input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('container'); ?>" name="<?php echo $this->get_field_name('container'); ?>" <?php checked( $container ); ?> />
		<label for="<?php echo $this->get_field_id('container'); ?>"><?php _e( 'Colocar dentro de um Container' ); ?></label><br />
		
		<p>Template a ser carregado (Use o formato loop-index)</p>
		<input type="text" name="<?php echo $this->get_field_name("templatename") ?>" value="<?php echo $instance['templatename'] ?>" id="<?php echo $this->get_field_id("templatename"); ?>">
		<?php if($instance['error'] != ""): ?>
			<div><strong>O arquivo não existe</strong></div>
		<?php endif; ?>
    <?php
	}

	function update($new_instance, $old_instance)
    {
		$instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
		$instance['templatename'] = trim($new_instance['templatename']);
		$instance['container'] = !empty($new_instance['container']) ? 1 : 0;
        return $instance;
	}
}
