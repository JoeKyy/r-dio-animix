<?php
Class superHighlights extends fksh_super_highlights{

	function __construct() {

	}

	function get_block( $block_slug = '' , $status = "" ){
		if( ! $block_slug ){
			return false;
		}
		$blocks = new fkdbt("fksh_superhighlights_blocks");
		$block = $blocks->find('first' , array(
			'conditions' => array(
				'slug' => $block_slug
			)
		));
		$block_id = $block->id;
		$total_frames = $block->frames;
		$block = $this->_get_block( $block_id , $status );
		$grouped_block = array();
		foreach( $block as $item ){
			$block_id = $item->block_id;
			$block_position = $item->block_position;
			$clean_name = str_replace("-$block_id-$block_position" , "", $item->option_name);
			if( $clean_name == "section_title" ){
				$grouped_block['section_title'] = html_entity_decode($item->option_value , ENT_NOQUOTES , "UTF-8");
				continue;
			}
			if( ($block_position > $total_frames) ){
				continue;
			}
			if( is_array( unserialize( $item->option_value) ) ){
				$value = unserialize( $item->option_value );
			}else{
				$value = html_entity_decode( $item->option_value , ENT_NOQUOTES , "UTF-8");
			}
			$grouped_block['slides'][$block_position][$clean_name] = $value;
		}
		return( $grouped_block );
	}

}

global $superHighlights;
$superHighlights = new superHighlights;

?>