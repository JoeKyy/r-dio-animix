<?php
/*
Plugin Name: FK Super Highlights
Version: 2.0
Plugin URI: http://www.filipekiss.com.br
Description: A plugin to make custom highlights for your blog.
Author: Filipe Kiss
Author URI: http://www.filipekiss.com.br
*/

Class fksh_super_highlights{

	var $form , $mainpage;

	function __construct($enable_menu = false) {
		register_activation_hook( __FILE__ , array( $this , 'activate' ) );
		$this->form = array(

			"section_title" => "text",
			"title" => array(
				"type" => "text",
				"attributes" => array(
					"class" => "input-large",
					"label" => "Título"
				)
			),
			"headline" => array(
				"type" => "text",
				"attributes" => array(
					"class" => "input-large",
					"label" => "Chapéu"
				)
			),
			"description" => array(
				"type" => "textarea",
				"attributes" => array(
					"class" => "input-large input-tall",
					"label" => "Linha-Fina"
				)
			),
			"url" => array(
				"type" => "text",
				"attributes" => array(
					"class" => "input-large",
					"label" => "URL"
				)
			),
			"image" => "image",
			"target" => array(
				"type" => "select",
				"values" => array(
					"_self" => "Na mesma janela/aba",
					"_blank" => "Nova janela/aba"
				),
				"attributes" => array(
					"label" => "Abrir o link"
				)
			)

		);
		if( $enable_menu ){
			add_action('admin_menu' , array( $this , 'admin_menu') );
		}
		add_action('wp_ajax_FKMH_get_image', array($this, '_get_image'));
	}

	function activate(){
		#$this->_setup_wp_options()
		$this->_setup_databases();
	}

	private function _setup_wp_options(){
		//Not used yet
	}

	private function _setup_databases(){

		if( ! class_exists( "fkdbt") ){
			trigger_error( "O Plugin FK Database tool é necessário para o funcionamento do FK Super Highlights", E_USER_ERROR);
		}
		if( class_exists( "fkdbt") ){
			$database_tool = new fkdbt( "fksh_superhighlights_blocks" );
		}

		$columns   = array();
		$columns[] = array( 'name' => 'slug'      , 'type' => 'varchar(45)'  , 'null' => false );
		$columns[] = array( 'name' => 'nice_name' , 'type' => 'varchar(150)' , 'null' => false );
		$columns[] = array( 'name' => 'frames'    , 'type' => 'int(2)'       , 'null' => false );
		$columns[] = array( 'name' => 'is_editable' , 'type' => 'varchar(50)' , 'null' => true );
		$columns[] = array( 'name' => 'preview_url' , 'type' => 'text' , 'null' => true );
		$columns[] = array( 'name' => 'box_width' , 'type' => 'varchar(10)' , 'null' => false );
		$columns[] = array( 'name' => 'box_height' , 'type' => 'varchar(10)' , 'null' => false );

		$database_tool->create_table( $columns );

		unset( $database_tool );

		$database_tool = new fkdbt( "fksh_superhighlights_formfields" );

		unset( $columns );

		$columns = array();
		$columns[] = array( 'name' => 'field_name' , 'type' => 'varchar(45)'  , 'null' => false );
		$columns[] = array( 'name' => 'field_type' , 'type' => 'varchar(45)'  , 'null' => false );
		$columns[] = array( 'name' => 'field_attributes' , 'type' => 'text' , 'null' => true);
		$columns[] = array( 'name' => 'field_values' , 'type' => 'text' , 'null' => true);
		$columns[] = array( 'name' => 'block_id'   , 'type' => 'mediumint(9)' , 'null' => false );

		$database_tool->create_table( $columns );

		unset( $columns , $database_tool );

		$database_tool = new fkdbt( "fksh_superhighlights_options" );

		$columns = array();
		$columns[] = array( 'name' => 'option_name' , 'type' => 'varchar(150)' , 'null' => false );
		$columns[] = array( 'name' => 'option_value' , 'type' => 'text' , 'null' => false );
		$columns[] = array( 'name' => 'option_status' , 'type' => 'varchar(10)' , 'null' => false );
		$columns[] = array( 'name' => 'block_id' , 'type' => 'int(10)' , 'null' => false );
		$columns[] = array( 'name' => 'block_position' , 'type' => 'int(10)' , 'null' => false );


		$database_tool->create_table( $columns );

	}

	function admin_menu(){
		$mainpage = add_menu_page( "Destaque", "Destaque", "edit_posts", "fk-super-highlights", array($this, "plugin_view"));
		$blocks = new fkdbt("fksh_superhighlights_blocks");
		add_action('admin_print_styles-'.$mainpage, array($this, 'admin_styles'));
		add_action('admin_enqueue_scripts', array($this, 'admin_scripts'));
		$total_blocks = (int) $blocks->find('count');
		if( $total_blocks > 0 ){
			foreach( $blocks->find('all') as $block ){
				$page = add_submenu_page("fk-super-highlights", $block->nice_name, $block->nice_name, "edit_posts", "fk-super-highlights-".$block->slug, array($this, "show_view"));
			add_action('admin_print_styles-'.$page, array($this, 'admin_styles'));
			}
		}
	}

	function admin_styles(){
		wp_enqueue_style('thickbox');
		wp_enqueue_style("super-highlights", fksh_SUPER_HIGHLIGHTS."/".'css'."/".'fk-multi-highlight.css');
		wp_enqueue_style("fk-icons", fksh_URL."/".'styles'."/".'fk-icons.css');
	}

	function admin_scripts($hook){
		if(stristr($hook, "super-highlights")){
			wp_enqueue_script('jquery');
			wp_enqueue_script('thickbox');
			wp_enqueue_script('media-upload');
			wp_enqueue_script('jquery-ui-sortable');
			wp_enqueue_script( "fkHighlightsBoxJs", fksh_SUPER_HIGHLIGHTS."/".'js'."/".'boxes.js', array('jquery','thickbox','media-upload', 'jquery-ui-sortable'));
			//Added some comment so we could deploy this normally
		}
	}

	function plugin_view(){
		if( @$_GET['action'] === "repair" ){
			?>
				<div class="wrap">
					<h2>FK Super Highlights</h2>
					<p> Executando o reparo do Banco de dados...</p>
			<?php
				$this->_setup_databases();
			?>
				<p>Reparo Concluído. Caso o erro persista, remova e instale o plugin novamente.</p>
				<p>
					<a href="admin.php?page=fk-super-highlights" class="button-primary">Continuar</a>
				</p>
			</div>
			<?php
			return;
		}
		?>
			<div class="wrap">
				<h2>Destaques</h2>
				<?php
				//Let's check if the tables were created
				global $wpdb;
				$table_exists = $wpdb->get_var("show tables like '".$wpdb->prefix."fksh_superhighlights_blocks'");
				if( ! $table_exists || $table_exists !== $wpdb->prefix."fksh_superhighlights_blocks" ){
					?>
					<h3>As tabelas do plugin não foram encontradas no banco de dados! Você precisa reparar seu banco de dados.</h3>
					<p>
						<a href="admin.php?page=fk-super-highlights&amp;action=repair" class="button-primary">Reparar Banco de Dados</a>
					</p>
					<?php
					return;
				}
				$blocks = new fkdbt("fksh_superhighlights_blocks");
				foreach( $blocks->find('all') as $block ){
					?>
						<p><a href="admin.php?page=fk-super-highlights-<?php echo $block->slug ?>"><?php echo $block->nice_name ?></a></p>
					<?php
				}
				$multi_available = get_option( "fk-multi-highlight-available" );
				if( ! empty( $multi_available ) ){
					?>
						<p><i>Foram encontrados registros do "FK Multi Highlights". Se desejar importar esses registros, clique <a href="admin.php?page=fk-super-highlights-import">aqui</a></i></p>
					<?php
				}
				?>
			</div>
		<?php
	}

	function import_screen(){
		?>
			<div class="wrap">
				<h2>FK Super Highlights - Importar</h2>
				<p>Os MultiHighlights abaixo foram encontrados. Basta selecionar para qual Super Highlight você deseja mandar o conteúdo e clicar em "Importar"</p>
				<?php

				$blocks = new fkdbt("fksh_superhighlights_blocks");
				add_action('admin_print_styles-'.$mainpage, array($this, 'admin_styles'));
				add_action('admin_enqueue_scripts', array($this, 'admin_scripts'));
				$total_blocks = (int) $blocks->find('count');
				if( $total_blocks > 0 ){
					foreach( $blocks->find('all') as $block ){
						$select .= "<option value='$block->id'>$block->nice_name</option>\n";
					}
				}
				$multi_available = get_option( "fk-multi-highlight-available" );
				foreach( $multi_available as $slug => $name ){
					?>
						<p>
							<label><?php echo $name ?></label>
						</p>
					<?php
					echo $slug." ".$name."<br />";
				}
				?>
			</div>
		<?php
	}

	function register_highlight( $slug = '' , $nice_name = '' , $slides = 0 , $box_size = '' , $form_array = '', $preview_url = '' ){
		if( is_array( $slug ) ){
			$attributes = (array) $slug;
			$slug = $attributes['slug'];
			$nice_name = $attributes['nice_name'];
			$slides = $attributes['slides'];
			$form_array = $attributes['form'];
		}
		if( ! $slug ){
			trigger_error( "Impossível criar um highlight sem definir os atributos" , E_USER_ERROR );
		}
		if( ! $nice_name || $slides < 0 ){
			trigger_error( "Ocorreu um erro ao tentar criar o highlight $slug. &Eacute; necess&aacute;rio um atributo 'nice_name'" , E_USER_ERROR );
		}

		$blocks = new fkdbt( "fksh_superhighlights_blocks" );
		$block = (array) $blocks->find('first' , array('conditions' => array( 'slug' => $slug ) ) );
		$block['slug'] = $slug;
		$block['nice_name'] = $nice_name;
		$block['frames']  = $slides;
		$block['is_editable'] = 'yes';
		$block['box_width'] = $box_size[0];
		$block['box_height'] = $box_size[1];
		$block['preview_url'] = $preview_url;
		if( $block['id'] ){
			$blocks->update( $block );
			$new_block = false;
		}else{
			$blocks->insert( $block );
			$block = (array) $blocks->find('first' , array('conditions' => array( 'slug' => $slug ) ) );
			$options = new fkdbt("fksh_superhighlights_options");
			$position = $block['frames']+1;
			$option = array();
			$option['option_name'] = 'section_title-'.$block['id']."-".$position;
			$option['option_value'] = $block['nice_name'];
			$option['block_id'] = $block['id'];
			$option['block_position'] = $position;
			$option['option_status'] = "1";
			$options->insert( $option );
		}

		//Now, to the forms!
		if( $form_array == "default" ){
			$form_array = $this->form;
		}

		$this->_save_form( $block['id'] , $form_array );
	}

	private function _save_form( $block_id = false , $form_array = false ){
		if( ! $block_id || ! is_array( $form_array ) ){
			return false;
		}
		$form_fields = new fkdbt("fksh_superhighlights_formfields");
		foreach( $form_array as $field_name => $attributes ){
			$attributes = (array) $attributes;
			$field_type = $attributes['type'];
			$field_values = ($attributes['values']) ? serialize( $attributes['values'] ) : false;
			$field_attributes = ($attributes['attributes']) ? serialize( $attributes['attributes'] ) : false;
			if( count( $attributes) == 1 ){
				$field_type = $attributes[0];
			}
			$field = (array) $form_fields->find('first' , array( 'conditions' => array( "AND" => array( 'field_name' => $field_name , 'block_id' => $block_id ) ) ) );
			$field['block_id'] = $block_id;
			$field['field_name'] = $field_name;
			$field['field_type'] = $field_type;
			$field['field_attributes'] = $field_attributes;
			$field['field_values'] = $field_values;
			if( $field['id'] ){
				$form_fields->update( $field );
			}else{
				$form_fields->insert( $field );
			}
		}
	}

	function show_view(){
		$block = str_replace("fk-super-highlights-" , "" , $_GET['page']);
		$blocks = new fkdbt("fksh_superhighlights_blocks");
		$block = $blocks->find( 'first' , array( 'conditions' => array( 'slug' => $block ) ) );
		if( isset( $_POST['_wpnonce'] ) && wp_verify_nonce($_POST['_wpnonce'], $block->slug) ) {
			//Received post. Let's save. (or preview, maybe)
			if( "salvar" == strtolower( $_POST['action'] ) ){
				$this->_save( $_POST );
				global $is_preview;
				$is_preview = false;
				$status = "1";
			}else{
				$this->_save( $_POST , "2" );
				global $is_preview;
				$is_preview = true;
				$status = "2";
			}
		}elseif( isset( $_GET['discard'] ) && $_GET['discard'] == "true" ){
			$this->_discard_changes( $block->id , "" , "" , "2" );
		}
		$status = ( $status ) ? $status : "1";
		?>
			<style>
			.box{
				float: left;
				width: <?php echo ($block->box_width) ?>px;
				height: <?php echo ($block->box_height) ?>px
			}

			.placehold{
				width: <?php echo ($block->box_width+6) ?>px;
				height: <?php echo ($block->box_height+6) ?>px
			}
			#loop-forms div{
				margin-top: <?php echo ($block->box_height+15) ?>px;
			}
			</style>
			<div class="wrap">
				<div class="icon-fksh icon32"><br /></div><h2><?php echo $block->nice_name; ?></h2>
				<p>Para alterar um destaque, clique na posição desejada e altere no formulário abaixo. Para alterar o título da seção, clique no título, acima dos quadros.</p>
				<?php if ($this->status != ""): ?>
					<div id="message" class="updated fade"><p><strong><?php echo $this->status ?></strong></p></div>
				<?php endif ?>
				<?php if ($is_preview): ?>
					<div id="message" class="updated fade"><p><strong>Atenção! As alterações não foram publicadas! Elas estão em Modo Preview! Para visualizar as alterações na página, clique <a href="<?php echo $block->preview_url; ?>?preview=true" target="preview">aqui</a></strong></p></div>
				<?php endif ?>
				<form method="post" action="admin.php?page=<?php echo $_GET['page']; ?>">
						<input type="submit" value="Salvar" name="action" class="button-primary" /> &nbsp; <input type="submit" name="action" value="Preview" class="button" /> &nbsp; <?php if( $is_preview ): ?><a class="button" href="admin.php?page=<?php echo $_GET['page'] ?>&discard=true">Descartar Alterações</a><?php endif; ?>
					<?php wp_nonce_field($block->slug); ?>
					<input type="hidden" name="block-id" value="<?php echo $block->id ?>" id="block-slug">
					<div class="title-holder">
						<h3 class="editable">
							<input type="text" name="section_title-<?php echo $block->id ?>-<?php echo $block->frames+1 ?>" class="readonly" value="<?php $title =  $this->_get_option('section_title' , $block->id, ($block->frames + 1) , $status); echo ( $title ? $title : $block->nice_name ); ?>" />
						</h3>
					</div>
					<div class="boxes-container" style="width: auto">
						<?php for($i = 1; $i <= $block->frames; $i++): ?>
							<div class="box" data-form="<?php echo $i ?>" data-boxid="<?php echo $i ?>">
								<?php $imagem = $this->_get_option('image' , $block->id , $i, $status); ?>
								<?php echo wp_get_attachment_image( $imagem['wp_image_id'] , array( $block->box_width, $block->box_height) ) ?>
							</div>
						<?php endfor; ?>
						<div class="clear"></div>
					</div>
					<div class="boxes-forms">
						<input type="hidden" name="<?php echo $block->slug ?>-save" value="save" />
						<input type="hidden" value="<?php echo $block->slug ?>" id="slugid" />
						<input type="hidden" rel="ordem" name="ordem" value="<?php $d = ""; for($i = 1; $i <= $block->frames; $i++){ $d .= $i.','; }; echo substr($d , 0, -1);?>" />
						<div id="loop-forms">
						    <?php for($i = 1; $i <= $block->frames; $i++): ?>
							    <div class="boxform<?php echo $i ?>" <?php echo "style=\"display: none\""; ?>>
								    <?php $this->_generate_form( $block , $i ); ?>
									<a style="float: right;" href="#" class="clear button">Limpar Tudo</a>
							    </div>
						    <?php endfor; ?>
						</div>
					</div>
				</form>
			</div>
		<?php
	}

	private function _generate_form( $block , $form_position ){
		global $is_preview;
		if( $is_preview ){
			$status = "2";
		}else{
			$status = "1";
		}
		$block_id = $block->id;
		$formfields = new fkdbt( "fksh_superhighlights_formfields" );
		$form = $formfields->find( 'all' , array( 'conditions' => array( 'block_id' => $block_id), 'order' => array('id') ) );
		?>
			<span class="close-boxform">Fechar</span>
			<h3><?php echo $block->nice_name ?> #<?php echo $form_position; ?></h3>
		<?php
		foreach( $form as $field ){
			if( $field->field_name != "section_title" ){
				$value = $this->_get_option( $field->field_name , $block_id, $form_position , $status );
				$this->_output_field( $field , $form_position , $value);
			}
		}
	}

	private function _output_field( $field_obj , $form_position , $value = '' ){
		$field_attributes = unserialize($field_obj->field_attributes);
		$field_label = $field_attributes['label'];
		if( ! $field_label ){
			$field_label = $field_obj->field_name;
		}
		switch( $field_obj->field_type ){
			case "textarea":
				?>
					<p>
						<?php echo $field_label; ?>
					</p>
					<p>
						<textarea name="<?php echo $field_obj->field_name."-".$field_obj->block_id."-".$form_position ?>" <?php $this->_output_attributes( $field_obj->field_attributes ); ?>><?php echo $value; ?></textarea>
					</p>
				<?php
			break;
			case "image":
				?>
					<p>
						<?php echo $field_label; ?>
					</p>
					<p>
						<input type="text" readonly="readonly" id="<?php echo $field_obj->field_name."_image_url-".$field_obj->block_id."-".$form_position ?>" name="<?php echo $field_obj->field_name."_image_url-".$field_obj->block_id."-".$form_position ?>" value="<?php echo @$value['wp_image_url'] ?>" />
						<input type="button" data-boxid="<?php echo $form_position ?>" data-displayfield="<?php echo $field_obj->field_name."_image_url-".$field_obj->block_id."-".$form_position ?>" data-formfield="<?php echo $field_obj->field_name."_image_id-".$field_obj->block_id."-".$form_position ?>" data-block="<?php echo $field_obj->block_id ?>" value="Enviar Imagem" class="custom_upload button" />
						<input type="hidden" name="<?php echo $field_obj->field_name."_image_id-".$field_obj->block_id."-".$form_position ?>" id="<?php echo $field_obj->field_name."_image_id-".$field_obj->block_id."-".$form_position ?>" value=<?php echo @$value['wp_image_id']; ?> />
					</p>
				<?php
			break;
			case "select":
				$options = unserialize( $field_obj->field_values );
				if( ! $options ){
					echo "";
				}else{
				?>
					<p>
						<?php echo $field_label; ?>
					</p>
					<p>
						<select name="<?php echo $field_obj->field_name."-".$field_obj->block_id."-".$form_position ?>" <?php $this->_output_attributes( $field_obj->field_attributes ); ?>>
							<?php
							foreach( $options as $opt_value => $text){
								?>
									<option value="<?php echo $opt_value ?>" <?php selected($opt_value , $value) ?>><?php echo $text ?></option>
								<?php
							}
							?>
						</select>
					</p>
				<?php
				}
			break;
			default:
				?>
					<p>
						<?php echo $field_label; ?>
					</p>
					<p>
						<input type="text" name="<?php echo $field_obj->field_name."-".$field_obj->block_id."-".$form_position ?>" <?php $this->_output_attributes( $field_obj->field_attributes ); ?> value="<?php echo $value ?>">
					</p>
				<?php
			break;
		}
	}

	private function _output_attributes( $attributes = '' )
	{
		if( ! $attributes ){
			return '';
		}
		$attributes = unserialize( $attributes );
		unset($attributes['label']);
		foreach( $attributes as $name => $value ){
			$attributes[$name] = "$name='$value'";
		}
		echo join( $attributes , " " );
	}

	public function _get_image()
	{
		$blocks = new fkdbt("fksh_superhighlights_blocks");
		$block = $blocks->find('first' , array( 'conditions' => array( 'id' => $_POST['slug'] ) ) );
		echo wp_get_attachment_image($_POST['image_id'], array( $block->box_width , $block->box_height) );
		die;
	}

	public function _save( $data , $status = "1")
	{
		//Let's validate if we are saving the right block
		$blocks = new fkdbt("fksh_superhighlights_blocks");
		$block = $blocks->find( 'first' , array( 'conditions' => array( 'id' => $_POST['block-id'] ) ) );
		if( $_POST[$block->slug."-save"] ==  "save" ){
			//Carregar os campos do Block pra poder localizar as variaveis no post
			$formfields = new fkdbt("fksh_superhighlights_formfields");
			$fields = $formfields->find('all' , array( 'conditions' => array( 'block_id' => $block->id ) ) );
			$ordem = explode("," , $_POST['ordem']);
			$counter = 1;
			foreach( $ordem as $position ){
				foreach( $fields as $field ){
					if( $field->field_type == "image" ){
						$value = array();
						$value['wp_image_id'] = $_POST[$field->field_name."_image_id-".$field->block_id."-".$position];
						$value['wp_image_url'] = $_POST[$field->field_name."_image_url-".$field->block_id."-".$position];
					}else{
						$value = $_POST[$field->field_name."-".$field->block_id."-".$position];
					}
					$this->_save_option($field->field_name , $field->block_id, $counter, $value, $status);
				}
				$counter++;
			}
			$value = $_POST["section_title-".$field->block_id."-".$counter];
			$this->_save_option("section_title" , $field->block_id, $counter, $value, $status);
		}else{
			global $status_message;
			$status_message = "Ocorreu um erro ao tentar salvar esse bloco. Por favor, tente novamente (erro 403)";
		}
		// print_r( $data );
	}

	function _save_option( $field_name , $block_id, $block_position, $value, $status = "1" ){
		$options = new fkdbt("fksh_superhighlights_options");
		$option = array();
		$option['option_name'] = $field_name."-".$block_id."-".$block_position;
		if( is_array( $value ) || is_object( $value) ){
			$value = serialize( $value );
		}else{
			$value = stripslashes(htmlentities(utf8_decode($value)));
		}
		$option['option_value'] = $value;
		$option['option_status'] = $status;
		$option['block_id'] = $block_id;
		$option['block_position'] = $block_position;
		$this->_discard_changes( $block_id , $block_position , $option['option_name'] , $status );
		return $options->insert( $option );
	}

	function _discard_changes( $block_id , $block_position , $option_name , $status = "1" ){
		global $wpdb;
		if( "" != $block_position ){
			$update_status_query = "UPDATE ".$options->table_name." SET option_status = 3 WHERE block_id = '$block_id' AND block_position = '$block_position' AND option_name = '$option_name' AND option_status = '$status'";
		}elseif( "" == $block_position && "" == $option_name ){
			$update_status_query = "UPDATE ".$options->table_name." SET option_status = 3 WHERE block_id = '$block_id' AND option_status = '$status'";
		}
		// echo ($update_status_query."\n");
		$wpdb->query( $update_status_query );
		if( "1" == $status){
			$this->_discard_changes( $block_id , $block_position , $option_name , "2" );
		}
	}

	function _get_option( $field_name , $block_id, $block_position, $status = "1"){
		$options = new fkdbt("fksh_superhighlights_options");
		$item = $options->find( 'first' , array(
			'conditions' => array(
				'AND' => array(
					'block_id' => $block_id,
					'block_position' => $block_position,
					'option_status' => $status,
					'option_name' => $field_name."-".$block_id."-".$block_position
				)
			),
			'order' => array(
				'id' => "DESC"
			)
		));
		if( is_array( unserialize( $item->option_value) ) ){
			$value = unserialize( $item->option_value );
		}else{
			$value = $item->option_value;
		}
		return $value;
	}

	function _get_block( $block_id , $status = "1" ){
		$options = new fkdbt("fksh_superhighlights_options");
		$block = $options->find( 'all' , array(
			'conditions' => array(
				'AND' => array(
					'block_id' => $block_id,
					'option_status' => $status,
				)
			),
			'order' => array(
				'id' => "ASC"
			)
		));
		if( !$block && $status == "2" ){
			$block = $options->find( 'all' , array(
				'conditions' => array(
					'AND' => array(
						'block_id' => $block_id,
						'option_status' => "1",
					)
				),
				'order' => array(
					'id' => "ASC"
				)
			));
		}
		return $block;
	}

	function _get_block_position( $block_id , $position ){
		$options = new fkdbt("fksh_superhighlights_options");
		$block = $options->find( 'all' , array(
			'conditions' => array(
				'AND' => array(
					'block_id' => $block_id,
					'option_status' => "1",
					'block_position' => $position
				)
			),
			'order' => array(
				'id' => "ASC"
			)
		));
		return $block;
	}
}

include 'super-highlights-functions.php';

if( !function_exists('urlpath')){
	function urlpath($path = ''){
		if( !$path ) $path = dirname(__FILE__);
		$this_dir = str_replace('\\','/', $path);
		return strrchr($this_dir, '/');
	}
}

define( 'fksh_URL' , WP_PLUGIN_URL.urlpath() );
define( 'fksh_SUPER_HIGHLIGHTS', WP_PLUGIN_URL.urlpath() );
