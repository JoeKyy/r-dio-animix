(function($){
	$(document).ready(function(){

		$(".boxes-container").sortable({
			cursor: 'move',
			opacity: 0.6,
			items: 'div.box',
			containment:  'parent',
			placeholder: 'placehold',
			update: function(event, ui){
				$list = new Array();
				$(this).children(".box").each(function(index, object){
					$list[index] = $(object).attr("data-form");
				});
				$position = new Array();
				$(this).children(".box").each(function(index, object){
					$position[index] = $(object).attr("data-boxid");
				});
				$("[rel=ordem]").val($position);
				$slugid = $("#slugid").val();
				$oldforms = $(this).siblings(".boxes-forms").children("#loop-forms").children("div");
				$oldforms = $(this).siblings(".boxes-forms").children("#loop-forms").children("div").remove();
				$($list).each(function(a, b){
					$newform = $oldforms.get(parseInt(b) - 1);
					$("#loop-forms").append(adjustName($newform, parseInt(parseInt(a) + 1)));
				});
				$(".boxes-container").children(".box").each(function(index, object){
					$(this).attr("data-form", parseInt(index) + 1);
				});
				var formposition = $(".box.active").offset();
				var boxform = "boxform"+$(".box.active").attr("data-form");
				$("."+boxform).css(formposition);
			}
		});

		function adjustName(formobj, newIndex){
			$myClass = $(formobj).attr("class");
			$newClass = $myClass.replace(/\d+/, newIndex);
			$(formobj).removeClass($myClass);
			$(formobj).addClass($newClass);
			$(formobj).find("h3").each(function(a,b){
					$(this).text($(this).text().replace(/\d+/, newIndex))
			})
			return $(formobj);
		}

		$(".boxes-container .box").live('click', function(){
			$(this).siblings(".active").removeClass("active");
			$(this).addClass("active");
			var formposition = $(this).position();
			var boxform = "boxform"+$(this).attr("data-form");
			calcPos($("."+boxform), formposition);
			$("."+boxform).siblings(".active").hide();
			$("."+boxform).addClass("active").show();
		});

		$('span.close-boxform').live('click', function(){
		    $(this).parent().hide();
		    $(".boxes-container .box").removeClass('active');
		});

	 $('.custom_upload').live('click', function() {
    	displayfield = jQuery(this).attr('data-displayfield');
	    formfield = jQuery(this).attr('data-formfield')
		box_id = jQuery(this).attr("data-boxid");
		post_slug = jQuery(this).attr("data-block")
	    tb_show('', 'media-upload.php?TB_iframe=true&type=image');
	    window.send_to_editor = function(html) {
	        post_id = html.match(/wp-image-([0-9]+)/)[1];
	        imgurl = jQuery(html).attr("src") ? jQuery(html).attr("src") : jQuery(html).find("img").attr("src");
	        jQuery('#' + displayfield).val(imgurl);
	        jQuery('#' + formfield).val(post_id);
			data = {
				action: "FKMH_get_image",
				image_id: post_id,
				slug: post_slug
			}
			jQuery.post(ajaxurl, data, function(data){
				jQuery(".box.active").html(data);
			});
	        tb_remove();
			return	false;
    	}
	});

	$(".clear.button").live('click' , function(e){
		e.preventDefault();
		$parent = $(this).parents(".active");
		// console.log($parent);
		$parent.find("input, textarea").each(function(){
			if( !$(this).hasClass("button") ){
				$(this).val("")
			}
		})
		$(".box.active").html("")
	})

	$(".editable input").click(function(){
		$(this).attr("readonly", "").removeClass("readonly");
	});

	$(".editable input").blur(function(){
		$(this).attr("readonly", "readonly").addClass("readonly");
	});

	function calcPos(obj, offset){
		$obj = obj;
		var maxPosition = $("body").outerWidth();
		var endPosition = offset.left+$obj.outerWidth();
		var finalLeftOffset = offset.left;
		while(endPosition > maxPosition){
			endPosition = endPosition - 50;
			finalLeftOffset = finalLeftOffset - 100;
		}
		myOffset = { left: finalLeftOffset, top: offset.top }
		$obj.css(myOffset);
	}

	});
})(jQuery);
