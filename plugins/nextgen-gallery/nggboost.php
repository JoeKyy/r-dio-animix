<?php
Class R7_NGG_Boost{
	
	var $images , $images_index , $counter , $id_index;
	
	function load_images( $images ){
		if( ! is_array( $images ) ){
			return false;
		}
		$this->images = $images;
	}
	
	function advance(){
		next( $this->images );
	}
	
	function reset(){
		reset( $this->images );
	}
	
	function prev( $current = '' ){
		if( ! $current ):
			$pi = prev( $this->images );
			if( $pi ){
				$this->advance();
			}else{
				$this->reset();
			}
			return $pi;
		else:
			return ( $this->images_index[$current]->prev ) ? $this->images_index[$current]->prev  : false;
		endif;
	}
	
	function restart(){
		$this->counter = 0;
		return true;
	}
	
	function create_index( &$counter = '' ){
		if( ! $counter ){
			$counter = &$this->counter;
		}
		if ( $counter ) {
			$counter++;
		}
		else{
			$counter = 1;
		}
		$current = $this->current();
		$prev = $this->prev();
		$next = $this->next();
		if( ! is_array( $this->id_index ) ){
			$this->id_index = array();
		}
		$this->id_index[$counter] = $current->pid; 
		$this->images_index[ $current->pid ] = new stdClass;
		$this->images_index[ $current->pid ]->prev = $this->images_index[ $current->pid ]->previous = $this->images_index[ $current->pid ]->previous_image = $prev->pid;
		$this->images_index[ $current->pid ]->next = $this->images_index[ $current->pid ]->next_image = $next->pid;
	}
	
	function next( $current = '' ){
		if( ! $current ):
			$ni = next( $this->images );
			return $ni;
		else:
			return ( $this->images_index[$current]->next ) ? $this->images_index[$current]->next  : false;
		endif;
	}
	
	function current(){
		$ci = current( $this->images );
		return $ci;
	}
	
	function get_index( $image_id = ''){
		if( ! $image_id ){
			return false;	
		}
		return ( array_search($image_id , $this->id_index) - 1);
	}
	
	function get_image( $image_id = '' ){
		return $this->images[$this->get_index( $image_id )];
	}
}

global $r7ngg;
$r7ngg = new R7_NGG_Boost;

?>
