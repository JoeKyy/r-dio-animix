<?php

/*
Plugin Name: Parceiros Animix
Description: Coloca os Parceiros da Rádio Animix
Author: Jhomar Nando
Version: 1
Author URI: http://jamesbruce.me/
*/


class ParceirosAnimix extends WP_Widget
{
  function ParceirosAnimix()
  {
    $widget_ops = array('classname' => 'ParceirosAnimix', 'description' => 'Coloca os Parceiros da Rádio Animix' );
    $this->WP_Widget('ParceirosAnimix', 'Animix - Parceiros', $widget_ops);
  }

  function form($instance)
  {
    $instance = wp_parse_args( (array) $instance, array( 'title' => '', 'url' => '', 'imageurl' => ''  ) );
    $title = $instance['title'];
    $url = $instance['url'];
    $imageurl = $instance['imageurl'];
?>
  <p><label for="<?php echo $this->get_field_id('title'); ?>">Nome do Parceiro: <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo attribute_escape($title); ?>" /></label></p>
  <p><label for="<?php echo $this->get_field_id('url'); ?>">Site do Parceiro: <input class="widefat" id="<?php echo $this->get_field_id('url'); ?>" name="<?php echo $this->get_field_name('url'); ?>" type="text" value="<?php echo attribute_escape($url); ?>" /></label></p>
  <p><label for="<?php echo $this->get_field_id('imageurl'); ?>">Imagem do Parceiro (80x30): <input class="widefat" id="<?php echo $this->get_field_id('imageurl'); ?>" name="<?php echo $this->get_field_name('imageurl'); ?>" type="text" value="<?php echo attribute_escape($imageurl); ?>" /></label></p>
<?php
  }

  function update($new_instance, $old_instance)
  {
    $instance = $old_instance;
    $instance['title'] = $new_instance['title'];
    $instance['url'] = $new_instance['url'];
    $instance['imageurl'] = $new_instance['imageurl'];
    return $instance;
  }

  function widget($args, $instance)
  {
    extract($args, EXTR_SKIP);

    echo $before_widget;
    $title = $instance['title'];
    $url = $instance['url'];
    $imageurl = $instance['imageurl'];

    // WIDGET CODE GOES HERE
    echo "<a title='". $title ."' href='". $url ."' target='_blank'><img src='". $imageurl ."' /></a>";

  }

}
add_action( 'widgets_init', create_function('', 'return register_widget("ParceirosAnimix");') );?>