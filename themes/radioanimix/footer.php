	<!-- FOOTER /-->
	<div id="footer">
	<div class="partners">
		<h6>Parceiros</h6>
		<ul>
			<?php if ( ! dynamic_sidebar( 'parceiros' ) ) : endif; ?>
		</ul>
	</div>
	<div class="sponsors">
		<h6>Apoio</h6>
		<div class="content-sponsors">
			<ul>
			<?php if ( ! dynamic_sidebar( 'patrocinadores' ) ) : endif; ?>
			</ul>
		</div>
	</div>
	<div class="copyright">
		<div class="content-copyright">
			<p><strong>Radio AniMIX</strong> &copy; 2013 - Há 9 anos com você - <a href="#">www.radioanimix.com.br</a></p>
			<cite>Todos os direitos reservados. É proibido a cópia de qualquer conteúdo deste site sem autorização formal do responsável.</cite>
			<ul>
				<li><strong>Desenvolvido por: </strong><a href="#">Jhomar JoeKyy</a></li>
				<li><strong>Desenvolvido por: </strong><a href="#">Jhomar JoeKyy</a></li>
			</ul>
		</div>
	</div>
	</div>
	<!-- /FOOTER /-->
	<?php wp_footer(); ?>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery-1.8.2.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.carouFredSel-4.5.1.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.functions.js"></script>
</body>
</html>
