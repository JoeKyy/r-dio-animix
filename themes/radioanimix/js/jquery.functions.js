$(document).ready(function(){

	$("#main #rotate ul").carouFredSel({
	infinite: true,
	circular: false,
	align: false,
	width: 620,
	height: 240,
	items: {
		visible: 1,
		width: 620,
		height: 620
	},
	scroll: {
		pauseOnHover: true,
		easing: "linear"
	},
	pagination: "#main #rotate .pagination"
	});

	$("#main #events ul").carouFredSel({
		items: {
			visible: 2,
			minimum: 2,
			width: 300,
			height: 200
		},
		scroll: {
			pauseOnHover: true,
			easing: "linear"
		},
		prev: "#main #events .eventArrowLeft",
		next: "#main #events .eventArrowRight"
	});

});