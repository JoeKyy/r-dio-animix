<?php
/*
Template Name: Em breve
*/
?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo( 'charset' ); ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>Rádio AniMiX - Em breve o novo site da Rádio AniMiX</title>
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php bloginfo('rss2_url'); ?>" />
	<link rel="alternate" type="text/xml" title="RSS .92" href="<?php bloginfo('rss_url'); ?>" />
	<link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="<?php bloginfo('atom_url'); ?>" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<?php
		if ( is_singular() && get_option( 'thread_comments' ) )
			wp_enqueue_script( 'comment-reply' );
		wp_head();
	?>
</head>
<body <?php body_class(); ?>>
	<!-- HEADER /-->
	<div id="header">

		<!-- LOGO -->
		<div id="logo">
			<?php if (is_home()) : ?>
				<h1><a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"><?php bloginfo( 'name' ); ?></a></h1>
			<?php else : ?>
				<h6><a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"><?php bloginfo( 'name' ); ?></a></h6>
			<?php endif; ?>
		</div>
		<!-- /LOGO -->
		<iframe src="http://animixplayer.com.br/player/top/player.php?cor=preto" style="position:fixed;" width="100%" scrolling="no" frameborder="0"></iframe>
	<!-- /HEADER /-->

<div id="main" class="soon">
	<div class="wrapper">
	<iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2FRadioAniMiX%3Ffref%3Dts&amp;width=800&amp;height=900&amp;show_faces=true&amp;colorscheme=light&amp;stream=true&amp;border_color&amp;header=true&amp;appId=142283455944006" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:800px; height:900px; margin: 250px 100px 0;" allowtransparency="true"></iframe>
	</div>
</div>
	<!-- FOOTER /-->
	<div id="footer">
	<div class="copyright">
		<div class="content-copyright">
			<p><strong>Radio AniMIX</strong> &copy; 2013 - Há 9 anos com você - <a href="http://www.radioanimix.com.br">www.radioanimix.com.br</a></p>
			<cite>Todos os direitos reservados. É proibido a cópia de qualquer conteúdo deste site sem autorização formal do responsável.</cite>
			<ul>
				<li><strong>Desenvolvido por: </strong><a href="http://www.joekyy.com.br">Jhomar Nando ~JoeKyy~</a></li>
				<li><strong>Design por: </strong><a href="http://www.felipekrust.com.br">Felipe Krust</a></li>
			</ul>
		</div>
	</div>
	</div>
	<!-- /FOOTER /-->
	<?php wp_footer(); ?>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery-1.8.2.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.carouFredSel-4.5.1.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.functions.js"></script>
</body>
</html>