<?php get_header(); ?>
<div id="main" class="internal">
	<div class="wrapper">
		<div id="content">
			<?php if ( !have_posts() ) get_template_part( 'not', 'found' ); ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php if(function_exists('breadcrumb')){ echo breadcrumb(); } ?>
					<!-- POST TITLE -->
					<h1><span>»</span> <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h1>
					<!-- /POST TITLE -->
					<div class="post-tools">
						<!-- DATETIME -->
						<var><?php the_time('d/m/Y') ?></var>
						<!-- /DATETIME -->
						<!-- AUTHOR -->
						<?php
							$autor = get_post_meta( get_the_ID(), 'author', true );
							if(!empty($autor)):
								echo "<address>Postado por: <em>".$autor."</em></address>";
							endif;
						?>
						<!-- /AUTHOR -->
						<!-- SOCIAL -->
						<div class="share">

							<!-- PLUGIN SENDMAIL -->
							<?php email_link(); ?>
							<!-- /PLUGIN SENDMAIL -->

						</div>
						<!-- /SOCIAL -->
					</div>
					<div class="entry">
						<?php the_content(); ?>
					</div>
				<?php endwhile; ?>
				<?php if(function_exists('wp_pagenavi') ): ?>
					<?php wp_pagenavi(); ?>
				<?php endif; ?>
		</div>
		<?php get_sidebar(); ?>
	</div>
	<div class="superbanner">
		<ul>
			<li class="left"></li>
			<li class="publicidade">
				<img src="http://placehold.it/728x90/000000/f4c40b&text=Anuncie%20aqui" alt="Publicidade">
			</li>
			<li class="right"></li>
		</ul>
	</div>
</div>
<?php get_footer(); ?>