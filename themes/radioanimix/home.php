
<?php get_header(); ?>
<div id="main">
	<div class="wrapper">
		<?php
			$highlight_status = ( @$_GET['preview'] == "true" ) ? "2" : "1";
			$home = $superHighlights->get_block( "tv-home" , $highlight_status );
			if(isset($home['slides'])):
		?>
		<div id="rotate">
			<ul>
		<?php
				foreach ($home['slides'] as $position => $slide):
					if( empty($slide['url']) || empty($slide['image']['wp_image_id']) ): continue; else:
		?>
				<li>
					<a href="<?php echo $slide["url"] ?>"  target="<?php echo $slide['target'] ?>"><?php echo wp_get_attachment_image($slide['image']['wp_image_id'], "size_620_240"); ?></a>
					<div class="caption">
						<span><?php echo $slide["headline"]; ?></span>
						<h2><a href="<?php echo $slide["url"] ?>"  target="<?php echo $slide['target'] ?>"><?php echo $slide["title"]; ?></a></h2>
					</div>
				</li>
		<?php 	endif;
				endforeach;
		?>
			</ul>
			<div class="pagination"></div>
		</div>
		<?php
			endif;
		?>
		<div id="newsAnimix">
			<h3>Novidades <i>Animix</i></h3>
			<a href="#" class="feed">Assine nosso feed</a>
			<ul>
				<?php
					$home = new WP_Query( array( 'posts_per_page' => '4') );
					while ( $home->have_posts() ) : $home->the_post();
				?>
				<li>
					<?php if( has_post_thumbnail() ): ?>
						<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark"><?php the_post_thumbnail('size_60_40'); ?></a>
					<?php endif; ?>
					<h4><a title="<?php the_title_attribute(); ?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
					<?php if(!empty($post->post_excerpt)):?>
						<p><a title="<?php the_title_attribute(); ?>" href="<?php the_permalink(); ?>"><?php echo get_the_excerpt();?></a></p>
					<?php endif; ?>
				</li>
				<?php endwhile; wp_reset_postdata(); ?>
			</ul>
		</div>
		<div id="news">
			<h3>Notícias <i>&</i> Matérias <i>recentes</i></h3>
			<ul>
				<?php
					$home = new WP_Query( array( 'posts_per_page' => '9') );
					while ( $home->have_posts() ) : $home->the_post();
				?>
				<li>
					<?php if( has_post_thumbnail() ): ?>
						<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark"><?php the_post_thumbnail('size_75_75'); ?></a>
					<?php endif; ?>
					<var><?php the_time('d/m/Y') ?></var>
					<h4><a title="<?php the_title_attribute(); ?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
					<?php if(!empty($post->post_excerpt)):?>
						<p><a title="<?php the_title_attribute(); ?>" href="<?php the_permalink(); ?>"><?php echo get_the_excerpt();?></a></p>
					<?php endif; ?>
				</li>
				<?php endwhile; wp_reset_postdata(); ?>
			</ul>
			<a href="<?php bloginfo('url') ?>/noticias" class="allNews"><b>+</b> Veja todas as notícias</a>
		</div>
		<div class="superbanner">
			<ul>
				<li class="left"></li>
				<li class="publicidade">
					<img src="http://placehold.it/728x90/000000/f4c40b&text=Anuncie%20aqui" alt="Publicidade">
				</li>
				<li class="right"></li>
			</ul>
		</div>
		<?php
				$home = $superHighlights->get_block( "eventos" , $highlight_status );
				if(isset($home['slides'])):
		?>
		<div id="events">
			<h3><i>Próximos</i> eventos <i>&</i> Shows</h3>
			<div class="eventArrowLeft"></div>
			<ul>
			<?php
					foreach ($home['slides'] as $position => $slide):
						if( empty($slide['url']) || empty($slide['image']['wp_image_id']) ): continue; else:
			?>
				<li>
					<a href="<?php echo $slide["url"] ?>"  target="<?php echo $slide['target'] ?>"><?php echo wp_get_attachment_image($slide['image']['wp_image_id'], "size_130_150"); ?></a>
					<h4><a href="<?php echo $slide["url"] ?>"  target="<?php echo $slide['target'] ?>"><?php echo $slide["title-evento"]; ?></a></h4>
					<p>
						<strong>Dias:</strong> <?php echo $slide["date-evento"]; ?><br />
						<strong>Local:</strong> <?php echo $slide["local-evento"]; ?><br />
						<strong>Atrações Principais:</strong><br />
						<?php echo $slide["highlights-evento"]; ?>
						<strong>Entrada:</strong> <?php echo $slide["entrada-evento"]; ?>
					</p>
				</li>
			<?php 	endif;
					endforeach;
			?>
			</ul>
			<div class="eventArrowRight"></div>
		</div>
		<?php
				endif;
		?>
		<div id="twitter">
			<a class="twitter-timeline" href="https://twitter.com/RadioAniMiX" data-widget-id="325480027703623680">Tweets de @RadioAniMiX</a>
			<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
		</div>
		<?php
				$home = $superHighlights->get_block( "interviews" , $highlight_status );
				if(isset($home['slides'])):
		?>
		<div id="interviews">
			<h3>Entrevistas <i>&</i> Galerias de Fotos</h3>
				<ul>
		<?php
					foreach ($home['slides'] as $position => $slide):
						if( empty($slide['url']) || empty($slide['image']['wp_image_id']) ): continue; else:
		?>
					<li>
						<div class="imageContent">
							<a href="<?php echo $slide["url"] ?>"  target="<?php echo $slide['target'] ?>"><?php echo wp_get_attachment_image($slide['image']['wp_image_id'], "size_270_170"); ?></a>
						</div>
						<h4><a href="<?php echo $slide["url"] ?>"  target="<?php echo $slide['target'] ?>"><?php echo $slide["title"]; ?></a></h4>
						<p><a href="<?php echo $slide["url"] ?>"  target="<?php echo $slide['target'] ?>"><?php echo $slide["description"]; ?></a></p>
					</li>
		<?php
						endif;
					endforeach;
		?>
				</ul>
			</h3>
		</div>
		<?php
				endif;
		?>
	</div>
</div>
<?php get_footer(); ?>