<?php

add_action( 'add_meta_boxes', 'add_postmetabox' );
add_action( 'save_post', 'save_postmetabox' );

function add_postmetabox(){
  add_meta_box('box_autor', 'Autor', 'box_autor_output', 'post', 'side', 'core');
}

function box_autor_output(){
  $value = ( isset( $_GET['post'] ) ) ? get_post_meta($_GET['post'], 'author', TRUE ) : '';
  echo '<input type="text" name="author" style="width:100%" maxlength="50" value="'.$value.'" />';
}

function save_postmetabox( $postid ){
  if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return $postid;

  update_post_meta($postid, 'author', $_POST['author']);
  return $postid;
}

// Tamanho de imagens
add_theme_support( 'post-thumbnails' );
add_image_size( 'size_140_90', 140, 90, true);
add_image_size( 'size_620_240', 620, 240, true);
add_image_size( 'size_270_170', 270, 170, true);
add_image_size( 'size_130_150', 130, 150, true);
add_image_size( 'size_75_75', 75, 75, true);
add_image_size( 'size_60_40', 60, 40, true);

/**
 * Troca o "[...]" colocado automaticamente pelo wordpress pelo caracter de reticências.
 *
 * ATENÇÃO: Não trocar o caracter reticências(…) por três pontos(...)!
 */
function theme_auto_excerpt_more( $more ) {
  return '…';
}
add_filter( 'excerpt_more', 'theme_auto_excerpt_more' );

/**
 * Define o resumo automático do blog para X palavras
 */
function theme_excerpt_length( $length ) {
  return 45;
}
add_filter( 'excerpt_length', 'theme_excerpt_length' );

/**
 * Função TrimText
 */
function trim_text( $text, $maxlength ) {

    if( strlen( $text ) <= $maxlength )
    return $text;

    $words  = explode( ' ', $text );
    $return = '';

    foreach ( $words as $word ) {
        if ( strlen( $return . ' ' . $word ) < $maxlength )
        $return .= ' ' . $word;
        else
        break;
    }

    return preg_replace( '/(\W|_)$/', '', $return ) . '…';
}

/**
 * Pega o texto do título do widget e o transforma em uma classe, para personalização individual
 *
 * ATENÇÃO: Para que o script funcione corretamete, os parâmetros de /before_title/ e /after_title/
 * da função register_sidebar devem ser vazios, para não gerar a tag duas vezes.
 */
function widget_title_as_class($title) {
  return '<div class="widget-title"><h3 class="' . sanitize_title($title) . '">' . $title . '</h3></div>';
}
add_filter('widget_title', 'widget_title_as_class');

/**
 * Registrando o menu
 */
register_nav_menu( 'principal', 'Menu principal' );

function add_menu_text_as_class($classes, $item){
  $classes[] = sanitize_title($item->title);
  return $classes;
}
add_filter('nav_menu_css_class', 'add_menu_text_as_class' , 10 , 2);

/**
 * Registra as áreas de widgets a serem usadas
 */
function theme_widgets_init() {
  register_sidebar( array(
    'name' => 'Sidebar',
    'id' => 'primary-widget-area',
    'description' => 'Área principal de widgets',
    'before_widget' => '<div id="%1$s" class="box widget-container %2$s">',
    'after_widget' => '</div>',
    'before_title' => '',
    'after_title' => '',
  ) );

  register_sidebar( array(
    'name' => 'Parceiros',
    'id' => 'parceiros',
    'description' => 'Área dos Parceitos',
    'before_widget' => '<li id="%1$s" class="box widget-container %2$s">',
    'after_widget' => '</li>',
    'before_title' => '',
    'after_title' => '',
  ) );

  register_sidebar( array(
    'name' => 'Patrocinadores',
    'id' => 'patrocinadores',
    'description' => 'Área dos Patrocinadores',
    'before_widget' => '<li id="%1$s" class="box widget-container %2$s">',
    'after_widget' => '</li>',
    'before_title' => '',
    'after_title' => '',
  ) );

}
/** Register sidebars by running theme_widgets_init() on the widgets_init hook. */
add_action( 'widgets_init', 'theme_widgets_init' );

//Criação dos destaques
if( class_exists( "fksh_super_highlights" ) ){

  $form = array(
            "title" => array("type" => "text","attributes" => array("class" => "input-large","label" => "Título")),
            "headline" => array("type" => "text","attributes" => array("class" => "input-large","label" => "Chapéu")),
            "url" => array("type" => "text","attributes" => array("class" => "input-large","label" => "URL")),
            "image" => "image",
            "target" => array("type" => "select","values" => array("_self" => "Na mesma janela/aba","_blank" => "Nova janela/aba"),"attributes" => array("label" => "Abrir o link"))
        );
  $box_size = array( 140 , 90 );
  $default_url = get_bloginfo('url');
  $home = new fksh_super_highlights(true);
  $home->register_highlight( 'tv-home' , 'Home > TV' , 4, $box_size, $form, $preview_url );
  $form = array(
            "title-evento" => array("type" => "text","attributes" => array("class" => "input-large","label" => "Nome do evento")),
            "date-evento" => array("type" => "text","attributes" => array("class" => "input-large","label" => "Data do evento")),
            "local-evento" => array("type" => "text","attributes" => array("class" => "input-large","label" => "Local do evento")),
            "highlights-evento" => array("type" => "textarea","attributes" => array("class" => "input-large input-tall","label" => "Atrações do evento (seja sucinto)")),
            "entrada-evento" => array("type" => "text","attributes" => array("class" => "input-large","label" => "Entrada do evento")),
            "url" => array("type" => "text","attributes" => array("class" => "input-large","label" => "URL")),
            "image" => "image",
            "target" => array("type" => "select","values" => array("_self" => "Na mesma janela/aba","_blank" => "Nova janela/aba"),"attributes" => array("label" => "Abrir o link"))
        );
  $home->register_highlight( 'eventos' , 'Home > Eventos' , 10, $box_size, $form, $preview_url );
  $form = array(
            "title" => array("type" => "text","attributes" => array("class" => "input-large","label" => "Título")),
            "description" => array("type" => "textarea","attributes" => array("class" => "input-large input-tall","label" => "Linha-Fina")),
            "url" => array("type" => "text","attributes" => array("class" => "input-large","label" => "URL")),
            "image" => "image",
            "target" => array("type" => "select","values" => array("_self" => "Na mesma janela/aba","_blank" => "Nova janela/aba"),"attributes" => array("label" => "Abrir o link"))
        );
  $home->register_highlight( 'interviews' , 'Home > Entrevistas e Galerias' , 2, $box_size, $form, $preview_url );
}

//POSTS RELACIONADOS POR TAGS
function relacionados_por_tags( $post_id ){
    $tags = wp_get_post_tags( $post_id );
    if ( $tags ) {
        $tag_ids = array();
        foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;

        $args=array(
            'tag__in' => $tag_ids,
            'post__not_in' => array( $post_id ),
            'showposts'=>4,
            'caller_get_posts'=>1
        );
    }

    $posts_relacionados = new WP_Query( $args );
    return $posts_relacionados;
}

if(!function_exists('breadcrumb')){
  function breadcrumb() {
    $delimiter = '>';
    $home = 'Página inicial'; // text for the 'Home' link
    $before = '<strong>'; // tag before the current crumb
    $after = '</strong>'; // tag after the current crumb

    if ( !is_home() && !is_front_page() || is_paged() ) {

      echo '<div id="breadcrumb"> você está em: ';

      global $post;
      $homeLink = get_bloginfo('url');
      echo '<a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . ' ';

      global $wp_query;
      global $wp;
      if ( is_category() ) {
        $cat_obj = $wp_query->get_queried_object();
        $thisCat = $cat_obj->term_id;
        $thisCat = get_category($thisCat);
        $parentCat = get_category($thisCat->parent);
        if ($thisCat->parent != 0) echo(get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' '));
        echo $before . single_cat_title('', false) . $after;

      } elseif ( is_day() ) {
        echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
        echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
        echo $before . get_the_time('d') . $after;

      } elseif ( is_month() ) {
        echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
        echo $before . get_the_time('F') . $after;

      } elseif ( is_year() ) {
        echo $before . get_the_time('Y') . $after;

      } elseif ( is_single() && !is_attachment() ) {
        if ( get_post_type() != 'post' ) {
          $post_type = get_post_type_object(get_post_type());
          $slug = $post_type->rewrite;
          echo '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a> ' . $delimiter . ' ';
          echo $before . get_the_title() . $after;
        } else {
          $cat = get_the_category(); $cat = $cat[0];
          echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
          echo $before . get_the_title() . $after;
        }

      } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
        $post_type = get_post_type_object(get_post_type());
        echo $before . $post_type->labels->singular_name . $after;

      } elseif ( is_attachment() ) {
        $parent = get_post($post->post_parent);
        $cat = get_the_category($parent->ID); $cat = $cat[0];
        echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
        echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a> ' . $delimiter . ' ';
        echo $before . get_the_title() . $after;

      } elseif ( is_page() && !$post->post_parent ) {
          echo $before . get_the_title() . $after;
      } elseif ( is_page() && $post->post_parent ) {
        $parent_id  = $post->post_parent;
        $breadcrumbs = array();
        while ($parent_id) {
          $page = get_page($parent_id);
          $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
          $parent_id  = $page->post_parent;
        }
        $breadcrumbs = array_reverse($breadcrumbs);
        foreach ($breadcrumbs as $crumb) echo $crumb . ' ' . $delimiter . ' ';
        echo $before . get_the_title() . $after;

      } elseif ( is_search() ) {
        echo $before . 'Resultados da busca para "' . get_search_query() . '"' . $after;

      } elseif ( is_tag() ) {
        echo $before . 'Posts com a tag "' . single_tag_title('', false) . '"' . $after;

      } elseif ( is_author() ) {
         global $author;
        $userdata = get_userdata($author);
        echo $before . 'Artigos postados por ' . $userdata->display_name . $after;

      } elseif ( is_404() ) {
        echo $before . 'Página não encontrada' . $after;
      }

      echo '</div>';

    }
  }
}

?>
